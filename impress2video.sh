#!/usr/bin/env bash

# Versión: 0.1
# Convierte una presentación de LibreOffice Impress a vídeo

ODP_FILE="$1"
TITLE="${1%.*}"
AUDIO_TRACK="$2"
DEPENDENCIES=()
OUTPUT="."
DELAY=3  # Retardo entre diapositivas (segundos)

_help() {
cat <<EOF
Uso: $0 ARCHIVO_ODT ARCHIVO_AUDIO
Convierte una presentación de LibreOffice Impress a vídeo.
Opciones:
    -h  Muestra esta ayuda y finaliza
EOF
}

_check_dependencies() {
    hash unoconv &> /dev/null || DEPENDENCIES+=(unoconv)
    hash pdftoppm &> /dev/null || DEPENDENCIES+=(poppler-utils)
    hash ffmpeg &> /dev/null || DEPENDENCIES+=(ffmpeg)
}

_odp_to_pdf_to_images() {
    # Sin el timeout aumentado da error en la primera ejecución
    unoconv --timeout=15 --format=pdf --output=./file.pdf "$ODP_FILE"
    mkdir -p ./images/
    pdftoppm -jpeg -jpegopt quality=90 ./file.pdf ./images/img
}

_images_to_video() {
    # Crea vídeo a partir de las imágenes
    # Opción -vf corrige el error 'height not divisible by 2' el el escalado
    # + Info: https://www.reck.dk/ffmpeg-libx264-height-not-divisible-by-2/
    ffmpeg -loglevel quiet -f image2 -r 1/"$DELAY" -i ./images/img-%02d.jpg \
        -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" "/tmp/tmp_video.mp4"
    # Duración del vídeo
    local duration=($(ffprobe /tmp/tmp_video.mp4 2>&1 | grep Duration))
    # Establece la duración de la pista de audio a la duración del vídeo
    ffmpeg -loglevel quiet -ss 00:00:00 -i "$AUDIO_TRACK" -t "${duration[1]::-1}" \
        -c:a libvorbis -qscale:a 5 -af "afade=out:st=66:d=7" "/tmp/audio_tmp.${AUDIO_TRACK##*.}"
    # Une las pistas de vídeo y audio
    ffmpeg -loglevel quiet -i "/tmp/tmp_video.mp4" -i "/tmp/audio_tmp.${AUDIO_TRACK##*.}" \
        -c:v copy -c:a copy "${OUTPUT}/${TITLE}.mp4"
}

# MAIN #

_check_dependencies

if [[ "${DEPENDENCIES[0]}" ]]; then
    echo -e "Las siguientes dependencias son necesarias:"
    echo "${DEPENDENCIES[*]}"
    echo  "Desea proceder a su instalación [s/N]?"
    while true; do
        read -r -p ": "
        case "$REPLY" in
            s|S)
                (sudo apt update -y && sudo apt install "${DEPENDENCIES[@]}" -y)
                break ;;
            n|N)
                echo "instalación cancelada."
                exit 1 ;;
            \?)
                echo "Opción no valida, indicar [S]i o [N]o"
        esac
    done
fi

echo "Convirtiendo archivo ODP..."
_odp_to_pdf_to_images
echo "Creando archivo de vídeo..."
_images_to_video
echo "Proceso finalizado"

